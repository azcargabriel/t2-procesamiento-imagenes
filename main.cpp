#define _DEBUG

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/calib3d/calib3d.hpp>

#include <iostream>

using namespace std;
using namespace cv;


Mat harrisFilter(Mat input) {
	Mat harris = Mat::zeros(input.rows, input.cols, CV_32FC1);

	Mat input_gray;
	cvtColor(input, input_gray, CV_BGR2GRAY);

	// 1) Suavizar la imagen de entrada
	//Blur the image with 5x5 Gaussian kernel
	Mat input_blurred;
	GaussianBlur(input_gray, input_blurred, Size(5, 5), 0);
	
	// 2) Calcular derivadas ix e iy
	Mat input_grad_x;
	Sobel(input_gray, input_grad_x, CV_32FC1, 1, 0);

	Mat input_grad_y;
	Sobel(input_gray, input_grad_y, CV_32FC1, 0, 1);

	// 3) Calcular momentos ixx, ixy, iyy
	//      ixx = ix*ix (elemento a elemento)
	//      ixy = ix*iy (elemento a elemento)
	//      iyy = iy*iy (elemento a elemento)
	Mat ixx = Mat::zeros(input_grad_x.rows, input_grad_x.cols, CV_32FC1);
	Mat ixy = Mat::zeros(input_grad_x.rows, input_grad_x.cols, CV_32FC1);
	Mat iyy = Mat::zeros(input_grad_y.rows, input_grad_y.cols, CV_32FC1);
	for (int r = 0; r < input_grad_x.rows; r++) {
		for (int c = 0; c < input_grad_x.cols; c++) {
			ixx.at<float>(r, c) = input_grad_x.at<float>(r,c) * input_grad_x.at<float>(r,c);
			ixy.at<float>(r, c) = input_grad_x.at<float>(r,c) * input_grad_y.at<float>(r,c);
			iyy.at<float>(r, c) = input_grad_y.at<float>(r,c) * input_grad_y.at<float>(r,c);
		}
	}

	// 4) Suavizar momentos ixx, ixy, iyy
	Mat ixx_blurred;
	GaussianBlur(ixx, ixx_blurred, Size(5, 5), 0);

	Mat ixy_blurred;
	GaussianBlur(ixy, ixy_blurred, Size(5, 5), 0);
	
	Mat iyy_blurred;
	GaussianBlur(iyy, iyy_blurred, Size(5, 5), 0);

	// 5) Calcular harris como: det(m) - 0.04*Tr(m)^2, con:
	//      m = [ixx, ixy; ixy, iyy]
	//      det(m) = ixx*iyy - ixy*ixy;
	//      Tr(m) = ixx + iyy

	float max = 0;
	for (int r = 0; r < harris.rows; r++) {
		for (int c = 0; c < harris.cols; c++) {
			float trm = ixx_blurred.at<float>(r,c) + iyy_blurred.at<float>(r,c);
			float det = ixx_blurred.at<float>(r,c)*iyy_blurred.at<float>(r,c) - ixy_blurred.at<float>(r,c)*ixy_blurred.at<float>(r,c);
			harris.at<float>(r,c) = det - 0.04 * trm * trm;
			if (max < harris.at<float>(r,c)) {
				max = harris.at<float>(r,c);
			}
		}
	}

	for (int r = 0; r < input.rows; r++) {
  		for (int c = 0; c < input.cols; c++) {
  			if (harris.at<float>(r, c) < 0){
  				harris.at<float>(r, c) = 0;
  			}
  			else {
  				harris.at<float>(r, c) *= (255.0 / max);
  			}
  		}
  	}

	// Ademas se debe transformar la imagen de harris para que quede en el rango 0-255
	Mat output;
	harris.convertTo(output, CV_8UC1);
	return output;
}

vector<KeyPoint> getHarrisPoints(Mat harris, int val) {
	vector<KeyPoint> points;
	for (int r = 1; r < harris.rows - 1; r++) {
		for (int c = 1; c < harris.cols - 1; c++) {
			int value = (int) harris.at<unsigned char>(r, c);
			
			//Si no supera el umbral
			if (value < val) {
				continue;
			}

			bool keyPoint = true;
			for(int w_r = -1; w_r < 2; w_r++) {
				for(int w_c = -1; w_c < 2; w_c++) {
					if (w_r == 0 && w_c == 0) {
						continue;
					}
					else if(value <= int(harris.at<unsigned char>(r + w_r, c + w_c))){
						//No es maximo asi que no es keypoint
						keyPoint = false;
					}
				}
			}

			if(keyPoint) {
				//Es maximo y supera el umbral
				points.push_back(KeyPoint(c, r, 1));
			}
		}
	}

	return points;
}

int main(void) {
	Mat imleft = imread("image_pairs/left1.png");
	Mat imright = imread("image_pairs/right1.png");

	if(imleft.empty() || imright.empty()) // No encontro la imagen
	{
		cout << "Imagen no encontrada" << endl;
		return 1;
	}

	Mat harrisleft, harrisright;	
	harrisleft = harrisFilter(imleft);
	harrisright = harrisFilter(imright);

	imwrite("harrisleft.jpg", harrisleft);
	imwrite("harrisright.jpg", harrisright);

	Ptr<ORB> orb = ORB::create();
	
	//Puntos de interes imagen izquierda
	vector<KeyPoint> pointsleft = getHarrisPoints(harrisleft, 40);
	Mat left_output;
	drawKeypoints(imleft, pointsleft, left_output);
	imwrite("impointsleft.jpg", left_output);

	//Puntos de interes imagen derecha
	vector<KeyPoint> pointsright = getHarrisPoints(harrisright, 40);
	Mat right_output;
	drawKeypoints(imright, pointsright, right_output);
	imwrite("impointsright.jpg", right_output);

	Mat descrleft, descrright;
	orb->compute(imleft, pointsleft, descrleft);
	orb->compute(imright, pointsright, descrright);

	//Puntos correspondientes entre imagenes
	BFMatcher matcher(NORM_HAMMING);
	std::vector<DMatch> matches;
	matcher.match(descrleft, descrright, matches);

	Mat imageMatches;   
	drawMatches(imleft,
				pointsleft,
				imright,
				pointsright,
				matches,
				imageMatches,
				cv::DrawMatchesFlags::DRAW_OVER_OUTIMG);
	imwrite("img_matches.jpg", imageMatches);

	//Pares de puntos correspondientes
	vector<Point2f> points1, points2;
	for (DMatch &dm : matches) {
		points1.push_back(pointsleft[dm.queryIdx].pt);
		points2.push_back(pointsright[dm.trainIdx].pt);
	}

	//Encontrar homografia y fusion de imagenes
	Mat h;
	h = findHomography(points1, points2, RANSAC);
	Mat imFused = Mat::zeros(imleft.rows*1.2, imleft.cols*2, CV_32FC1);
  	warpPerspective(imright, imFused, h, imFused.size(), CV_WARP_INVERSE_MAP);

  	for (size_t r = 0; r < imleft.rows; r++) {
		for (size_t c = 0; c < imleft.cols * 3; c++) {
			imFused.at<unsigned char>(r, c) = imleft.at<unsigned char>(r, c);
		}
	}

	imwrite("fused.jpg", imFused);

	return 0;
}

